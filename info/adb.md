#### Kill running app
`adb shell am force-stop [app package]`  
Also good for killing off running instrumentation tests

#### Disable animations
```
adb shell settings put global window_animation_scale 0
adb shell settings put global transition_animation_scale 0
adb shell settings put global animator_duration_scale 0
```
