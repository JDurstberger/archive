#Python

##VirtualEnv

Virtual environments are used to run a project with a decoupled interpreter
and installed dependencies. Therefore 2 projects can depend on the same dependency
but on different versions when they both use their on venv.

###Create VirtualEnv

Make sure pip is installed and up to date.
Make sure virtualenv is installed and up to date.

```
virtualenv env
```
where env will be the name of the directory. You can add -p python3 to create
the environment with the python3 interpreter.

Since version 3.6 python ships with 'venv' which can be called like

```
python3 -m venv env
```

###Activate VirtualEnv

`source /path/to/venv/bin/activate`

You should now see '(env)' in front of your bash cursor

###Deactivate VirtualEnv

`deactivate`

Yep that was easy!

###Install requirements

`pip install -r requirements.txt`
