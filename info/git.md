#### Show message of all dangling commits

git fsck --lost-found | grep commit | awk '/dangling commit/ {print $3}' | xargs -n1 git log -1 --oneline
